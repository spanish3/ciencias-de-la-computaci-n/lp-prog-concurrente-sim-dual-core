package tp3;

import java.io.PrintWriter;

public class Log extends Thread{

	 private int cont = 0;

	 private PrintWriter pw; //hara el log general
	 private PrintWriter pw2; //Hara log de transiciones para T-Invariantes

	 private int sleepTime;
	 private PetriNet Rdp;


	 public Log(PrintWriter pw, PrintWriter pw2, PetriNet Rdp, int i)
	 {
		 this.pw = pw;
		 this.pw2 = pw2;
		 this.sleepTime = i;
		 this.Rdp = Rdp;
	 }
	 
	 public void run() 
	 {
		 while (!(isInterrupted())) 
		 {
			 pw.printf("Log %d: \n" , cont);
			 pw.printf("Tareas en buffer 1: %d \n", Rdp.getMarcas(2));
			 pw.printf("CPUPower1 state: %s - %s \n", Rdp.getCPUStateArray()[0].getON_OFF_State(), Rdp.getCPUStateArray()[0].getActivityState());
			 pw.printf("Tareas en buffer 2: %d \n", Rdp.getMarcas(9));
			 pw.printf("CPUPower2 state: %s - %s \n", Rdp.getCPUStateArray()[1].getON_OFF_State(), Rdp.getCPUStateArray()[1].getActivityState());
			 pw.printf("Tareas completadas: CPU1: %d , CPU2: %d\n", Rdp.getProcesadasB1() , Rdp.getProcesadasB2());
			 try 
			 {
				 sleep(sleepTime);
			 } 
			 catch (InterruptedException ex) 
			 {
				 if(ex.getMessage().equals("sleep interrupted"))
				 {
					 return;
				 }
			 }
			 cont++;
		 }
	 }
	 
	 public void end(float tiempo)
	 {
		 interrupt();
		 pw.printf("\n\n\nArrival Rate: %d - Service Rate CPU1: %d - Service Rate CPU2: %d \n" , Rdp.getVentana(0).getAlpha() , Rdp.getVentana(3).getAlpha() , Rdp.getVentana(10).getAlpha()); 
		 pw.printf("Elementos generados: %d\n"  , Rdp.getIngresadasB1() + Rdp.getIngresadasB2());
		 pw.printf("BufferCPU1: %d  - BufferCPU2: %d\n"  , Rdp.getIngresadasB1() , Rdp.getIngresadasB2());
		 pw.printf("Elementos procesados: Cpu1: %d , Cpu2: %d\n", Rdp.getProcesadasB1() , Rdp.getProcesadasB2());
		 pw.printf("Tiempo Final de ejecucion: %f: \n" , tiempo);
		 pw.printf("P-Invariant Test result: %s \n" , Boolean.toString(Rdp.isPinvariant()));
		 if(!Rdp.isPinvariant())
		 {
			 pw.printf("P-Invariant Log: %s \n" , Rdp.getPinvariantLog()); 
		 }
		 pw2.print(Rdp.getTinvariantLog());
	 }
}