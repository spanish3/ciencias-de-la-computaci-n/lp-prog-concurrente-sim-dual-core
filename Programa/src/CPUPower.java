package tp3;

public class CPUPower extends Thread 
{	
	private int id;
	private Monitor m;
	private PetriNet Rdp;
	
	public CPUPower(int id, Monitor m, PetriNet Rdp)
	{
		this.id = id;
		this.m = m;
		this.Rdp = Rdp;
	}
	
	public void run()
	{
		while(!this.Rdp.getTerminado())
		{
			if(!this.Rdp.getTerminado())
			{
				if(this.id == 0)
				{
					try 
		        	{
						this.m.entroydisparo(7);
						this.m.entroydisparo(5);						
						this.m.entroydisparo(4);
		            } 
		        	catch (InterruptedException e) 
		        	{
		        		System.out.printf("FALLO DEL GENERADOR\n");
		                return;
		        	}
				}
				if(this.id == 1)
				{
					try 
		        	{
						this.m.entroydisparo(14);
						this.m.entroydisparo(12);
						this.m.entroydisparo(11);
		            } 
		        	catch (InterruptedException e) 
		        	{
		        		System.out.printf("FALLO DEL GENERADOR\n");
		                return;
		        	}
				}
			}
		}
	}
}
