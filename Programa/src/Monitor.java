package tp3;

import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Monitor 
{
	private ReentrantLock lock = new ReentrantLock(true);
	private LinkedList<Condition> waisStack = new LinkedList<>();
	private int transiciones;
	private PetriNet Rdp;
	private Politica p;
	private boolean asleepThreads[];
	private boolean isSleepingAlpha[];
	
	public Monitor(PetriNet Rdp, Politica p, int t)
	{
		this.Rdp = Rdp;
		this.p = p;
		this.transiciones = t;
		for(int i = 0; i< this.transiciones; i++)	// Se crean tantas colas de condicion como transiciones.
		{
			waisStack.add(lock.newCondition());
		}
		asleepThreads = new boolean[this.transiciones];
		for (int i = 0; i< transiciones ; i++)
		{
			asleepThreads[i] = false;
		}
		isSleepingAlpha = new boolean[this.transiciones];
		for (int i = 0; i< transiciones ; i++)
		{
			isSleepingAlpha[i] = false;
		}
	}
	
	public void entroydisparo(int transicion) throws InterruptedException
	{
			int awake;
			long alpha;
			long ahora;
			long timestamp;
			long sleeptime;
			try
			{
				lock.lock();
				while(!Rdp.esSensibilizada(transicion))
				{
					if(this.Rdp.getTerminado())
					{
						break;
					}
					asleepThreads[transicion] = true;
					waisStack.get(transicion).await();
				}
				if(!this.Rdp.getTerminado())
				{
					if(Rdp.esTemporizada(transicion))
					{
						alpha = Rdp.getVentana(transicion).getAlpha();
						timestamp = Rdp.getTimeStamp(transicion);
						ahora = System.currentTimeMillis();
						if(alpha > (ahora - timestamp))
						{
							sleeptime = alpha - (ahora - timestamp);
							isSleepingAlpha[transicion] = true;
							lock.unlock();
							Thread.sleep(sleeptime);
							lock.lock();
							isSleepingAlpha[transicion] = false;
						}
					}
					asleepThreads[transicion] = false;
					Rdp.disparo(transicion);
					Rdp.printMarcado();
					System.out.print("Procesadas CPU1:  "+Rdp.getProcesadasB1()+" Procesadas CPU2:  "+Rdp.getProcesadasB2()+"\n");
					awake = p.tADisparar(asleepThreads,isSleepingAlpha);  // Preguntar a la pol�tica que hilo despertar
					waisStack.get(awake).signal();
				}
				if(this.Rdp.getTerminado())
				{
					for(int i = 0; i < this.transiciones; i++)
					{
						if(asleepThreads[i])
						{
							waisStack.get(i).signal();  
						}
					}
				}
			}
			finally 
			{
				//Al cumplirse la condicion de disparo se dispara la transicion y se vuelve a la secuencia de codigo
				//mientras se sigue estando dentro del monitor (lock.lock).
				lock.unlock();
			}
	}

}
