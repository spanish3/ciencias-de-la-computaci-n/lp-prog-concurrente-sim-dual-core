package tp3;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Main 
{
	
	public static void main(String[] args) 
	{
		int CANT_PLAZAS = 16;
		int CANT_TRANS = 15;
		int CANT_DATOS = 1000;
		
		int LOG_DELAY = 100;

		//Variables para el calcuo del tiempo
		float tiempoFinal;
		long tTotal;
		long tInicio =  System.currentTimeMillis();
		
		String LOG_FILE = "log.txt";
		String TINV_LOG = "tinvariantes.txt";
		
		FicherosExcel lFich = new FicherosExcel("PetriNet.xls",CANT_PLAZAS,CANT_TRANS);

		PetriNet RdP = new PetriNet(lFich,CANT_DATOS);
		
		Politica p = new Politica(RdP,CANT_TRANS);
		
		Monitor m = new Monitor(RdP, p,CANT_TRANS);
		
		Thread.currentThread().setName("MAIN");
		
		System.out.printf("Main crea los hilos\n");
		
		CPUPower cpower[] = new CPUPower[2];
		cpower[0] = new CPUPower(0, m, RdP);
		cpower[1] = new CPUPower(1, m, RdP);
		
		CPUTask ctask[] = new CPUTask[2];
		ctask[0] = new CPUTask(0, m, RdP);
		ctask[1] = new CPUTask(1, m, RdP);
		
		CPUStay_ON cpuon[] = new CPUStay_ON[2];
		cpuon[0] = new CPUStay_ON(0, m, RdP);
		cpuon[1] = new CPUStay_ON(1, m, RdP);
		
		ArrivalData a[] = new ArrivalData[2];
		a[0] = new ArrivalData(0, RdP, m);
		a[1] = new ArrivalData(1, RdP, m);
		
		GeneradorTareas g = new GeneradorTareas(m, CANT_DATOS);
		
		//Elementos para el LOG
		FileWriter file = null;
		PrintWriter pw = null;
		try
		{
			file = new FileWriter(LOG_FILE);
			pw = new PrintWriter(file);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		//Log de T-Invariantes
		FileWriter file2 = null;
		PrintWriter pw2 = null;
		try
		{
			file2 = new FileWriter(TINV_LOG);
			pw2 = new PrintWriter(file2);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		Log log = new Log(pw , pw2 ,RdP ,LOG_DELAY);
		
		// Iniciar Hilos
		System.out.print("MAIN: Se inician los hilos\n");

		for(int i = 0 ; i < 2 ; i++)
		{
			cpower[i].setName("CP" + (i +1));
			cpower[i].start();
			cpuon[i].setName("CPON" + (i +1));  // Clase CPUStay_ON
			cpuon[i].start();
			a[i].setName("AD" + (i+1));
			a[i].start();
			ctask[i].setName("CC" + (i+1));
			ctask[i].start();
		}

		g.setName("GEN");
		g.start();
		
		log.start();
		
		try {g.join();} catch (InterruptedException e) {e.printStackTrace();}
		
		// Esperar que finalicen los hilos restantes (o equivalentemente, que se terminen de procesar las tareas)
		try 
		{
			for(int i = 0 ; i < 2 ; i++)
			{
				cpower[i].join();
				ctask[i].join();
				a[i].join();
			}
		}catch (InterruptedException e) {e.printStackTrace();}
		
		tTotal = System.currentTimeMillis() - tInicio;
		tiempoFinal = tTotal/ 1000F;
		
		System.out.printf("MAIN: ");
		RdP.printMarcado();
		
		log.end(tiempoFinal);

		// Esperar hasta que termine de imprimir el log
		try{
			log.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// Cerrar los archivos correspondientes
		try{
			if(null != file && null != file2)
				file.close();
				file2.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("MAIN: Tiempo total: "+tiempoFinal);
		System.out.print("MAIN: Fin de la ejecucion\n");
	}

}
