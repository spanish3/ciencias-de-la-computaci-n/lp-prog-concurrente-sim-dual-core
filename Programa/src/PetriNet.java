package tp3;

public class PetriNet 
{
    private FicherosExcel f;
	private int[] marcado;
    private int[][] matrizIE;         
    private int[][] matrizIS;
    private int[][] Inhibition;
    private boolean[] sensibilizadas;
    
    private int plazas, transiciones;
    private int IngresadasB1, IngresadasB2;
    private int ProcesadasB1, ProcesadasB2;
    private int MAX;
    private CPUState c[];
    
    private Ventana v[];
    private long timeStamps[];
    
    private String pinvariantLog = "";
    private boolean pinvariant ;

    private String tinvariantLog = "";
    
    public PetriNet(FicherosExcel f, int m)
    {
    	this.f = f;
    	this.plazas = f.getPlazas();
    	this.transiciones = f.getTransiciones();
    	this.matrizIE = f.getMatriz(0);
    	this.matrizIS = f.getMatriz(1);
    	this.Inhibition = f.getMatriz(3);
    	this.marcado = f.getMarcadoInicial();
    	this.sensibilizadas = new boolean[this.transiciones];
    	this.ProcesadasB1 = 0;
    	this.ProcesadasB2 = 0;
    	this.MAX = m;
        c = new CPUState[2];
        c[0] = new CPUState();
        c[1] = new CPUState();
        this.timeStamps = new long[transiciones];
        this.v = f.getVentana();
    	sensibilizar();
    	// Descomentar para visualizar lo obtenido del archivo excel
    	//this.printMarcado();
        //this.printSensibilizadas();
        //this.ImprimirMIE();
        //this.ImprimirMIS();
    	//this.printVentana();
    }
    
    //Para sensibilizar las transiciones iterara sobre el marcado actual y la matriz de Incidencia negativa
    //siguiendo el lineamiento logico de la ecuacion fundamental de redes de petri.
    public void sensibilizar()
    {
    	for(int i = 0 ; i < this.transiciones ; i++)
    	{
    		for(int j = 0; j <this.plazas ; j++)
    		{
    			if (!(marcado[j] >= matrizIE[j][i])) 
    			{  
    				sensibilizadas[i] = false;
    				break;
    			}
    			sensibilizadas[i] = true;
    			if(Inhibition[j][i] == 1)
    			{
    				if(marcado[j] != 0)
        			{
        				sensibilizadas[i] = false;
        				break;
        			}
    			}
             }
         }
    }

    //El disparo de una transicion corresponde a la modificacion del marcado actual
    //a partir de la matriz de incidencia combinada Ic = (I+) + (I-).
    public void disparo(int transicion)
    {
    	boolean EA[] = new boolean[this.transiciones], EP[] = new boolean[this.transiciones];
    	for(int i = 0; i < this.transiciones ; i++)
    	{
    		if(this.esTemporizada(i))
    		{
    			EA[i] = this.esSensibilizada(i);
    		}
    		else
    		{
    			EA[i] = true;
    		}
    	}
    	//System.out.println("Disparo transicion "+transicion);
    	for(int j = 0 ; j < this.plazas ; j++)
    	{
    		marcado[j] += matrizIS[j][transicion];
        }
        for(int j = 0 ; j < this.plazas ; j++)
        {
            marcado[j] -= matrizIE[j][transicion];
        }
        sensibilizar();
        for(int i = 0; i < this.transiciones ; i++)
    	{
    		if(this.esTemporizada(i))
    		{
    			EP[i] = this.esSensibilizada(i);
    		}
    		else
    		{
    			EP[i] = false;
    		}
    		if(!EA[i] && EP[i])  // Si las transiciones sensibilizadas NO estaban sensibilizadas y ahora luego del disparo lo están
    		{
    			this.timeStamps[i] = System.currentTimeMillis();	// Empezar a contar el tiempo para poder dispararlas
    		}
    	}
        ContarTareas(transicion);	
        pinvariantTest(transicion);
        addTinvariantLog(transicion); 
        ActualizarEstado(transicion);  // Mecanismo para terminar la ejecución con el marcado inicial.
    }
    
    public long getTimeStamp(int t)
    {
    	return this.timeStamps[t];
    }
    
    public boolean esTemporizada (int t)
    {
    	if(this.v[t].getAlpha() != 0)
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }
    
    public Ventana getVentana (int t)
    {
    	return this.v[t];
    }

    public void ContarTareas(int t)
    {
    	if(t == 1)
        {
        	this.IngresadasB1++;
        }
        if(t == 8)
        {
        	this.IngresadasB2++;
        }
    	if(t == 3)
        {
        	this.ProcesadasB1++;
        }
        if(t == 10)
        {
        	this.ProcesadasB2++;
        }
    }
    
    public int getIngresadasB1 ()
    {
    	return this.IngresadasB1;
    }
    public int getIngresadasB2 ()
    {
    	return this.IngresadasB2;
    }
    public int getProcesadasB1 ()
    {
    	return this.ProcesadasB1;
    }
    public int getProcesadasB2 ()
    {
    	return this.ProcesadasB2;
    }
    
    public void ActualizarEstado(int t)
    {
    	if(t == 5 || t == 6)
    	{
    		c[0].setON_OFF_State("ON");
    	}
    	if(t == 2)
    	{
    		c[0].setActivityState("ACTIVE");
    	}
    	if(t == 3)
    	{
    		c[0].setActivityState("IDLE");
    	}
    	if(t == 4)
    	{
    		c[0].setON_OFF_State("STAND_BY");
    	}
    	if(t == 12 || t == 13)
    	{
    		c[1].setON_OFF_State("ON");
    	}
    	if(t == 9)
    	{
    		c[1].setActivityState("ACTIVE");
    	}
    	if(t == 10)
    	{
    		c[1].setActivityState("IDLE");
    	}
    	if(t == 11)
    	{
    		c[1].setON_OFF_State("STAND_BY");
    	}
    }
    
    public CPUState[] getCPUStateArray()
    {
    	return this.c;
    }
    
    public boolean esSensibilizada(int i)
    {
    	return this.sensibilizadas[i];
    }
    
    public int getMarcas(int Plaza)
    {
    	return this.marcado[Plaza];
    }
    
    public boolean getTerminado()
    {
    	if(((this.ProcesadasB1 + this.ProcesadasB2) == this.MAX) && ((c[0].getON_OFF_State().equals("STAND_BY")) && (c[1].getON_OFF_State().equals("STAND_BY"))))
    	{
    		return true;
    	}
    	return false;
    }
    
    //Para analizar invariantes
    
    public void pinvariantTest(int t)
    {
        if((marcado[0] + marcado[1]) != 1)
        {
        	pinvariant = false;
        	pinvariantLog = pinvariantLog + "Fallo en P-Invariante: M(P0) + M(P1) = 1\n Luego del disparo de T" + t + "\n";
        }
        if((marcado[3] + marcado[4]) != 1)
        {
        	pinvariant = false;
        	pinvariantLog = pinvariantLog + "Fallo en P-Invariante: M(P3) + M(P4) = 1\n Luego del disparo de T" + t + "\n";
        }
        if((marcado[5] + marcado[6] + marcado[8]) != 1)
        {
        	pinvariant = false;
        	pinvariantLog = pinvariantLog + "Fallo en P-Invariante: M(P5) + M(P6)+ M(P8) = 1\n Luego del disparo de T" + t + "\n";
        }
        if((marcado[10] + marcado[11]) != 1)
        {
        	pinvariant = false;
        	pinvariantLog = pinvariantLog + "Fallo en P-Invariante: M(P10) + M(P11) = 1\n Luego del disparo de T" + t + "\n";
        }
        if((marcado[12] + marcado[13] + marcado[15]) != 1)
        {
        	pinvariant = false;
        	pinvariantLog = pinvariantLog + "Fallo en P-Invariante: M(P12) + M(P13)+ M(P14) = 1\n Luego del disparo de T" + t + "\n";
        }
        pinvariant = true;
    }
    
    public boolean isPinvariant() {return pinvariant;}

    public String getPinvariantLog(){return pinvariantLog;}
    
    public void addTinvariantLog(int trans) {tinvariantLog = tinvariantLog +"T" + trans + "-";}
    
    public String getTinvariantLog(){return tinvariantLog;}
    
    // Utilizado para Debug
    
    public void printSensibilizadas()
    {
    	for(int i =0; i < this.sensibilizadas.length; i++)
    	{
    		if(this.sensibilizadas[i])
    		{
    			System.out.println("transición "+i+" sensibilizada");
    		}
    		else
    		{
    			System.out.println("transición "+i+" no sensibilizada");
    		}
    	}
    }
    
    public void ImprimirMIE()
    {
    	for(int i =0; i < this.sensibilizadas.length; i++)
    	{
    		for(int j =0; j < this.sensibilizadas.length; j++)
    		{
    			System.out.printf("M ["+i+"]["+j+"] = "+this.matrizIE[i][j]);
    		}
    		System.out.println();
    	}
    	System.out.println();
    }
    
    public void ImprimirMIS()
    {
    	for(int i =0; i < this.plazas; i++)
    	{
    		for(int j =0; j < this.transiciones; j++)
    		{
    			System.out.printf("M ["+i+"]["+j+"] = "+this.matrizIS[i][j]);
    		}
    		System.out.println();
    	}
    	System.out.println();
    }
    
    public void printMarcado() 
    {
    	System.out.printf("Marcado Actual: ");
    	for (int i = 0; i < marcado.length; i++) 
        {
            System.out.printf("%d, ", marcado[i]);
        }
        System.out.println();
    }
    
    public void printVentana()
	{
		for(int i = 0; i < this.transiciones; i++)
		{
			System.out.println("T"+i+" Alpha: "+this.v[i].getAlpha()+" Beta: "+this.v[i].getBeta());
		}
	}
    
    // Getter nunca utilizado, solo existe para deshacerse del warning
    public FicherosExcel getFicherosExcel() {return this.f;};

}