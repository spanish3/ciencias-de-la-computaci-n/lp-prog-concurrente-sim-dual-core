package tp3;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class FicherosExcel {

	String NombreArchivo;
	int nPlazas;
	int nTransiciones;
	
	public FicherosExcel(String n, int p, int t)
	{
		this.NombreArchivo = n;
		this.nPlazas = p;
		this.nTransiciones = t;
	}
	
	public int[] getMarcadoInicial()
	{
		int[] marcado = new int[this.nPlazas];
		try(FileInputStream file = new FileInputStream(new File(this.NombreArchivo)))
		{	
			// Se crea el objeto HSSF para manejar XLS de Excel
			HSSFWorkbook workbook = new HSSFWorkbook(file);
			
			// Obtener tercera planilla
			HSSFSheet sheet = workbook.getSheetAt(2);
			
			//Iterar Fila a fila y celda a celda
			Iterator<Row> rowIt = sheet.iterator();
			int i = 0;
			while(rowIt.hasNext())
			{
				Row row = rowIt.next();
				Iterator<Cell> cellIterator = row.cellIterator();
				Cell cell = cellIterator.next();
				marcado[i] = (int)cell.getNumericCellValue();
				i++;
			}
			workbook.close();
			file.close();
		}catch (Exception e){}
		return marcado;
	}
	
	public int[][] getMatriz(int p)
	{
		int[][] matriz = new int[this.nPlazas][this.nTransiciones];
		try(FileInputStream file = new FileInputStream(new File(this.NombreArchivo)))
		{	
			// Se crea el objeto HSSF para manejar XLS de Excel
			HSSFWorkbook workbook = new HSSFWorkbook(file);
			
			// Obtener primera planilla
			HSSFSheet sheet = workbook.getSheetAt(p);
			
			//Iterar Fila a fila y celda a celda
			Iterator<Row> rowIt = sheet.iterator();
			Row row;
			int i = 0;
			while(rowIt.hasNext())
			{
				int j = 0;
				row = rowIt.next();
				Iterator<Cell> cellIterator = row.cellIterator();
				Cell cell;
				while(cellIterator.hasNext())
				{
					cell = cellIterator.next();
					matriz[i][j] = (int)cell.getNumericCellValue();
					if(j < this.nTransiciones)
						j++;
				}
				if(i < this.nPlazas)
					i++;
			}
			workbook.close();
			file.close();
		}catch (Exception e){}
		return matriz;
	}
	
	public Ventana[] getVentana()
	{
		Ventana v[] = new Ventana[this.nTransiciones];
		try(FileInputStream file = new FileInputStream(new File(this.NombreArchivo)))
		{	
			// Se crea el objeto HSSF para manejar XLS de Excel
			HSSFWorkbook workbook = new HSSFWorkbook(file);
			
			// Obtener primera planilla
			HSSFSheet sheet = workbook.getSheetAt(4);	// La cuarta hoja del archivo contiene la ventana de las transiciones
			
			//Iterar Fila a fila y celda a celda
			Iterator<Row> rowIt = sheet.iterator();
			Row row;
			while(rowIt.hasNext())
			{
				int j = 0;
				row = rowIt.next();
				Iterator<Cell> cellIterator = row.cellIterator();
				Cell cell;
				while(cellIterator.hasNext())
				{
					cell = cellIterator.next();
					if(row.getRowNum() == 0)
					{
						v[j] = new Ventana();
						v[j].setAlpha((long)cell.getNumericCellValue());
					}
					if(row.getRowNum() == 1)
					{
						v[j].setBeta((long)cell.getNumericCellValue());
					}
					
					if(j < this.nTransiciones)
						j++;
				}
			}
			workbook.close();
			file.close();
		}catch (Exception e){}
		return v;
	}
	
	
	public int getPlazas()
	{
		return this.nPlazas;
	}
	
	public int getTransiciones()
	{
		return this.nTransiciones;
	}
	
}