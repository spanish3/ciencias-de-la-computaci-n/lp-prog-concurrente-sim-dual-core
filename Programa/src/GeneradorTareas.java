package tp3;

public class GeneradorTareas extends Thread{
    private Monitor m;
    private int cont = 0;
    private int MAX;
    
    public GeneradorTareas(Monitor m, int max) 
    {
        super();
        this.m = m;
        this.MAX = max;
    }
    
    public void run() 
    {
        while (cont < MAX) 
        {
        	try 							
        	{
        		m.entroydisparo(0);			
            } 								
        	catch (InterruptedException e) 
        	{
        		System.out.printf("FALLO DEL GENERADOR\n");
                return;
        	}
            cont++;
        }
        System.out.printf("GENERADOR: He terminado de generar %d datos\n", cont);
    }
}