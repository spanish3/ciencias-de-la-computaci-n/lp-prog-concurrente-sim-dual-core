package tp3;


public class Politica {
	private PetriNet Rdp;
	private int transiciones;
	private boolean flag;
	private boolean[] T_awake;
    
	   public Politica(PetriNet Rdp, int t)	// cuidado, se debe despertar un solo hilo, una sola transicion
	   {
		   this.transiciones = t;
		   this.Rdp = Rdp;
		   this.flag = false;
		   T_awake = new boolean[this.transiciones];
	   }
	   
	    public void fireableTransitions()
	    {
	    	for(int i = 0; i <transiciones; i++)
	    	{
	    		if(this.Rdp.esSensibilizada(i))
	    		{
	    			T_awake[i] = true;
	    		}
	    		else
	    		{
	    			T_awake[i] = false;
	    		}
	    	}
	    	if(this.Rdp.esSensibilizada(1)&&this.Rdp.esSensibilizada(8))
	    	{
		    	flag = !flag;
		    	if(flag)
		        {
		    		T_awake[8] = false;	// Solo se despierta el hilo que dispara T1
		    		if(this.Rdp.getMarcas(2) > this.Rdp.getMarcas(9))
		    		{
		    			// Solo se despierta el hilo que dispara T8
		    			T_awake[1] = false;
		    			T_awake[8] = true;	
		    		}
		        }
		        else
		        {
		        	T_awake[1] = false;  // Solo se despierta el hilo que dispara T8
		    		if(this.Rdp.getMarcas(2) < this.Rdp.getMarcas(9))
		    		{
		    			//Solo se despierta el hilo que dispara T1
		    			T_awake[1] = true;
		    			T_awake[8] = false;	
		    		}
		        }
	    	}
	    }
	    
	    public int tADisparar(boolean asleepThreads[], boolean isSleepingAlpha[])
	    {
	    	fireableTransitions();
	    	for (int i = 0; i < transiciones; i++)	// Se despertará el primer hilo dormido correspondiente a una transición disparable
	    	{										// Disparable: Intenta disparar una transición sensibilizada, elegida por la política en caso de conflicto 
	    		if (T_awake[i] && asleepThreads[i] && (!isSleepingAlpha[i])) // y que no se encuentre dormido por cuestiones de temporización
	    		{
	    			return i;
	    		}
	    	}
	    	return 0;
	    }
}
