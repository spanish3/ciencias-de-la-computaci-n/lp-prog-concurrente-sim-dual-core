package tp3;

public class CPUTask extends Thread 
{
	private int id;
	private Monitor m;
	private PetriNet Rdp;
	
	public CPUTask(int id, Monitor m, PetriNet Rdp)
	{
		this.id = id;
		this.m = m;
		this.Rdp = Rdp;
	}
	
	public void run()
	{
		while(!this.Rdp.getTerminado())
		{
			if(!this.Rdp.getTerminado())
			{
				if(this.id == 0)
				{
					try 
		        	{
						this.m.entroydisparo(2);
						this.m.entroydisparo(3);
		            } 
		        	catch (InterruptedException e) 
		        	{
		        		System.out.printf("FALLO DEL GENERADOR\n");
		                return;
		        	}
				}
				if(this.id == 1)
				{
					try 
		        	{
						this.m.entroydisparo(9);
						this.m.entroydisparo(10);	
		            } 
		        	catch (InterruptedException e) 
		        	{
		        		System.out.printf("FALLO DEL GENERADOR\n");
		                return;
		        	}
				}
			}
		}
	}
}
