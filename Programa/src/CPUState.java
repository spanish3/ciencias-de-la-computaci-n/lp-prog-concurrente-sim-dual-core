package tp3;

public class CPUState 
{
	private String on_off;
	private String activity;
	
	public CPUState()
	{
		this.on_off = "STAND_BY";
		this.activity = "IDLE";
	}
	
	public String getON_OFF_State()
	{
		return this.on_off;
	}
	
	public void setON_OFF_State(String s)
	{
		this.on_off = s;
	}
	public String getActivityState()
	{
		return this.activity;
	}
	
	public void setActivityState(String s)
	{
		this.activity = s;
	}
}
