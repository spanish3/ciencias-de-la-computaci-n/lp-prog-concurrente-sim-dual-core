# Programación Concurrente - Trabajo Práctico 3

Año 2021

## Clonar Repositorio

```bash
cd existing_repo
git remote add origin https://gitlab.com/spanish3/ciencias-de-la-computaci-n/lp-prog-concurrente-sim-dual-core.git
git branch -M main
git pull origin main
```

## Enunciado

Se debe implementar un simulador de un procesador con dos núcleos. A partir de la red de Petri de la figura 1, la cual representa a un procesador mono núcleo, se deberá extender la misma a una red que modele un procesador con dos núcleos. Además, se debe implementar una Política que resuelva los conflictos que se generan con las transiciones que alimentan los buffers de los núcleos (CPU\_Buffer). 

![](img/Picture1.png)

Nota: Las transiciones “Arrival\_rate” y “Service\_rate” son temporizadas y representan el tiempo de arribo de una tarea y el tiempo de servicio para concluir una tarea. 

## Red de Petri

Teniendo como base la Red de Petri del enunciado, se extendió la misma para simular 2 CPUs según se pide en el enunciado del trabajo. Con el software PIPE se obtuvo la siguiente red: 

![](img/Picture2.png)

## Descripción de plazas y transiciones

Luego de haber realizado la red, nos quedan las siguientes descripciones para las plazas y transiciones 

|**Plazas** |||
| - | :- | :- |
|**Número** |**Nombre** |**Descripción** |
|P0 |Data\_gen |El generador de datos está listo para generar. |
|P1 |Data\_ready |Se ha generado un dato, listo para enviarlo a un CPU |
|P2 |CPU\_Buffer\_1 |Cantidad de datos esperando en el buffer 1 |
|P3 |Active1 |El CPU 1 está procesando un dato. |
|P4 |Idle1 |El CPU 1 está ocioso (no está procesando). |
|P5 |CPU\_ON\_1 |El CPU 1 está encendido. |
|P6 |Power\_Up\_1 |El CPU 1 está encendido. |
|P7 |Data\_In\_1 |Entró un nuevo dato en el buffer del CPU 1 |
|P8 |Stand\_By\_1 |El CPU 1 está en Stand by. |
|P9 |CPU\_Buffer\_2 |Cantidad de datos esperando en el buffer 2 |
|P10 |Active2 |El CPU 2 está procesando un dato. |
|P11 |Idle2 |El CPU 2 está ocioso (no está procesando). |
|P12 |CPU\_ON\_2 |El CPU 2 está encendido. |
|P13 |Power\_Up\_2 |El CPU 2 se está encendiendo. |
|P14 |Data\_In\_2 |Entró un nuevo dato en el buffer del CPU 2 |
|P15 |Stand\_By\_2 |El CPU 2 está en Stand by. |


|**Transiciones** |||
| - | :- | :- |
|**Número** |**Nombre** |**Descripción** |
|T0 |Arrival\_rate |Tiempo que tarda el generador en generar un dato. |
|T1 |Arrival\_Data\_1 |Entra un dato en el buffer del CPU 1. |
|T2 |st\_ch1 |CPU1 toma un dato de su buffer y lo procesa. |
|T3 |Service\_rate\_1 |Tiempo que tarda el CPU1 en procesar un dato. |
|T4 |Pwr\_dwn\_th1 |CPU1 pasa de estado Encendido a Stand By. |
|T5 |Pw\_up\_dl1 |Tiempo que tarda el CPU1 en encenderse. |
|T6 |stay\_on1 |Mantener encendido el CPU si se está procesando un dato y hay elementos en el buffer. |
|T7 |stand\_by\_dl1 |Tiempo que tarda el CPU1 en pasar de Stand\_by a encendido. |
|T8 |Arrival\_Data\_2 |Entra un dato en el buffer del CPU 2. |
|T9 |st\_ch2 |CPU 2 toma un dato de su buffer y lo procesa. |
|T10 |Service\_rate\_2 |Tiempo que tarda el CPU 2 en procesar un dato. |
|T11 |Pwr\_dwn\_th2 |CPU 2 pasa de estado Encendido a Stand By. |
|T12 |Pw\_up\_dl2 |Tiempo que tarda el CPU 2 en encenderse. |
|T13 |stay\_on2 |Mantener encendido el CPU si se está procesando un dato y hay elementos en el buffer. |
|T14 |stand\_by\_dl2 |Tiempo que tarda el CPU 2 en pasar de Stand\_by a encendido. |

## Resultados de la clasificación de la RdP

![](img/Picture3.png)

### Propiedades 

- Es viva. 
- Es sin interbloqueo. 
- Es reversible. 
- No es libre de conflicto, tiene un conflicto estructural en P1(T1,T8). 
- Acotada o limitada: No  
- Segura: No. 
- Simple: No 
- Simple Extendida: No 
- Pura: No 
- De Libre elección:No 
- De libre elección extendida: No 
- Autónoma: Si 
- Ordinaria: Si 
- Pseudo viva: Si 
- Quasi viva: Si 
- HomeState: Si y es reversible 
- No es un gráfico marcado. 

## Matrices

Se tienen las matrices de incidencia de entrada MIE, incidencia de salida MIS, incidencia combinada e inhibición que son utilizadas en el programa para la actualización del marcado de la red (cuando se dispara una transición se le debe restar tantas marcas a cada plaza que tiene un arco saliente a dicha transición como el peso del arco lo indique, esto es un elemento de MIE, y sumarle tantas marcas a las plazas que tengan un arco entrante proveniente de dicha transición como este otro arco lo indique, el peso de este arco es un elemento de MIS) y para determinar si una transición está o no sensibilizada (la matriz de incidencia de entrada tiene como elementos los pesos de los arcos que van desde las plazas a las transiciones, luego si la cantidad de marcas en cada una de las plazas que tienen un arco hacia una transición es igual o mayor al peso del arco, dicha transición está sensibilizada. Además, se debe cumplir lo análogo para arcos inhibidores). 

### Matriz de Incidencia de Entrada 

![](img/Picture4.png)

### Matriz de Incidencia de Salida

![](img/Picture5.png)

### Matriz de Incidencia Combinada 

![](img/Picture6.png)

### Matriz de Inhibición

![](img/Picture7.png)

## Estructura general del programa 

Se realizó el programa en el lenguaje de programación Java como se nos pide en el enunciado, dentro del mismo se utilizó la siguiente estructura de clases:

**Main:** Es el encargado de la creación de los objetos que serán necesarios para el programa y de la ejecución del resto de los 6 hilos que tendremos.

**Monitor:** Se utiliza un monitor para controlar los distintos disparos de las transiciones en nuestra red de Petri. Para asegurar que no se disparen dos transiciones al mismo tiempo se utilizó un ReentrantLock. Luego se implementó un Condition para mantener en espera a los hilos que quieran disparar. Una analogía de esto sería el control del ingreso a una casa, y el condition nos daría las habitaciones dentro de la casa para esperar a su turno de realizar la tarea dentro de la casa.

**PetriNet:** es la clase que modela nuestra Red de Petri, la misma se levantará desde un archivo de Excel, de forma que puedan conformarse distintas configuraciones de la Red de acuerdo a los cambios que hagamos en la planilla de Excel. Dentro de esta tendremos las matriz de incidencia de salida y entrada, el marcado actual y las transiciones sensibilizadas. En base a esto podremos verificar las distintas posibilidades de disparos dentro de la Red. Por último, nos da la posibilidad de actualizar la Red luego de haber realizado un disparo.

**FicherosExcel:** Esta clase es la que lee un archivo de excel para luego pasar esa información a la clase PetriNet. Convertirá los valores de las celdas en matrices de enteros o un arreglo en caso del marcado actual. Dentro del archivo de excel tenemos distintas pestañas correspondientes a las distintas matrices y marcado. Se utilizó la librería poi que nos brinda herramientas para recorrer los archivos por celdas y columnas. Como nombramos previamente, modificando los archivos de Excel podremos cambiar la Red de Petri.

**Log:** En esta clase iremos plasmando el funcionamiento del programa y en las condiciones que está cada un determinado tiempo, mostrando en este archivo de texto el tamaño de los buffers, los estados de los mismos e información general del programa.

**CPUPower:** Hace referencia al prendido y apagado de ambos CPUs, correrá un hilo para cada uno de ellos, quedando al final dos hilos de este tipo. Cuando entra un nuevo dato pasaremos al estado Power\_Up, en el caso contrario de que no haya elementos en el buffer para procesar pasaremos al estado de Stand\_By. Con esto simularemos un sistema en el cual buscamos mantener prendido el procesador solo cuando se utiliza, para de esta forma reducir el consumo.

**CPUTask:** Se encarga propiamente del “procesamiento” de los datos, simula eso mismo. Como la clase anterior. también corre dos hilos, será uno para cada procesador. Se encargará de disparar a través del monitor a las transiciones T2 y T3 (cpu1) y T9 y T10 (cpu2).

**CPU\_State:** Nos devuelve los distintos estados de los CPUs 

**GeneradorDatos:** Es la clase encargada de crear los datos que irán ingresando al sistema. Controlará que sea una cantidad menor a un valor máximo establecido y en base a una política destinará el dato a uno de los dos procesadores 

**Política:** Como nombramos previamente, esta clase establece una regla para decidir a cuál de los dos procesadores irán los datos generados. Deberá comprobar el estado de los buffers, las transiciones disparables y las despertadas.

**CPU_StayON:** Esta clase será la encargada de mantener encendido el procesador una vez que ya está iniciado, hace referencia en particular a las transiciones T6 y T13, las cuales son stay_on1 y stay_on2 respectivamente. La misma comprobará si la RdP término la ejecución, de no ser así, disparara las dos transiciones de acuerdo con el id de buffer que corresponda.

**ArrivalData:** Se tendrán dos objetos de esta clase, cada uno de los cuales constituirá un hilo del programa. Estos hilos intentarán llevar una tarea al buffer del procesador 1 y procesador 2 respectivamente. Esto implica un conflicto que será resuelto en el monitor, despertando uno de los estos dos hilos (que se encontrarán dormidos en colas de condición distintas) de acuerdo a lo que indique la política. 

**Ventana:** En esta clase tendremos los setters y getters de alpha y beta, siendo estos los valores
necesarios para implementar las transiciones temporizadas utilizadas. Los mismos los obtendremos de
un archivo de Excel donde seleccionamos los valores para alpha y beta correspondientes al arrival_rate
y service_rate de cada procesador.

### Diagrama de Clases 

![](img/Picture8.png)

A continuación, se muestra el diagrama de secuencias que muestra la resolución del conflicto generado por las transiciones T1 y T8. Para ello inicialmente GeneradorTareas generará una tarea para que los procesadores realicen, es decir disparará T0 (Nótese que T0 es temporizada, con lo cual se incluye el funcionamiento correspondiente. T1 y T8 no son temporizadas por lo cual, si bien dicho comportamiento sucede cada vez que se intenta disparar una transición no se mostrará en el diagrama para estas transiciones). Luego del disparo de T0, se pregunta a la política cuál transición disparar y esta responderá con una transición que cumpla las siguientes condiciones: Que un hilo haya intentado disparar y ahora se encuentre esperando en su cola de condición, que este hilo NO se encuentre dormido por cuestiones de temporización, que la transición esté sensibilizada y que haya sido elegida en caso de conflicto. La política será descripta luego del diagrama.

## Diagrama de Secuencia 

![](img/Picture9.png)
![](img/Picture10.png)
![](img/Picture11.png)

## Política
La política retornará la primera transición que esté sensibilizada [A], que haya un hilo que haya intentado dispararla esperando en su cola de condición [B] (no dormido por cuestiones de transiciones temporizadas [C]). Además, resolverá el conflicto cuando lo haya [D] de la siguiente manera: Para evitar la congestión o el uso ineficiente de los procesadores se implementó una política para distribuir el procesamiento de los datos entre los dos procesadores. Buscamos que no se sobrecargue ninguno de los buffers. En el caso de que los dos procesador procesaran el dato a exactamente la misma velocidad, bastaría con simplemente hacer que vaya un dato para cada procesador alternando entre ellos. Tendremos problemas cuando tengamos distintos sv_rate en los procesadores, haciendo que uno de los dos sea más rápido y por lo tanto se generan sobrecargas en algún momento. Para solucionar esto, lo que se realizó es una política que en un principio distribuiría un dato a la vez en cada procesador, luego revisará el estado de ambos buffers, en caso de que uno de los dos tenga menor carga que el otro, se le destinaría el dato a ese, de lo contrario, seria con el otro procesador. De esta forma, logramos equilibrar las cargas en los dos procesadores. A continuación, podemos ver la implementación de la política en código. El método fireableTransitions se encargará de [A] y [D] y el método pADisparar verificará [B] y [C]. Este último retornará la transición correspondiente al hilo que debe despertarse.

![](img/Picture12.png)
![](img/Picture13.png)

## Desarrollo de la Red de Petri

Como nombramos previamente, nuestra Red de Petri se implementó mediante la clase PetriNet en java. Ésta nos permite a partir de un archivo de Excel analizar y modificar la Red y su marcado de acuerdo con las transiciones que son disparadas. Nuestra clase tiene las siguientes funciones: 

**sensibilizar():** Con esta función determinaremos si las distintas transiciones están sensibilizadas o no. Sobre un arreglo booleano, nos devolverá en cada posición del mismo (cada una de las transiciones) si es que están sensibilizadas o no. Se implementa comparando la matriz de inhibición con el marcado, siendo que tendría que ser 0 para ese momento y comparando la matriz de incidencia de entrada también con el marcado, si es que el marcado es mayor en esa posición, entonces la transición está sensibilizada.

**esSensibilizada():** nos devuelve si la transición que le pasamos como parámetro está sensibilizada o no.

**disparo():** Como indica su nombre, realiza el disparo de una transición determinada. Haciendo esto, en la matriz de incidencia de salida se le sumará el valor correspondiente del marcado, también se le restará el valor correspondiente a la matriz de incidencia de entrada. De esta forma se va actualizando la Red de acuerdo a cada disparo que ocurre.

**pinvariantTest():** Se utiliza para realizar el análisis de los p-invariantes. La misma tiene dentro distintas if que comprueben el cumplimiento de los p-invariantes obtenidos con la herramienta PIPE, de no cumplirse, pone un valor booleano de p-invariantes en false y agrega al archivo que luego se imprimirá un texto avisando que hubo un fallo 

**esTemporizada():** Devuelve un valor booleano indicando si la transición por la que se pregunta es temporizada o no. Este factor nos será de gran importancia para saber cómo tratar las transiciones de acuerdo a si son temporizadas o no.

**ActualizarEstado():** De acuerdo a la transición que le es pasada determinara el estado en el cual se encuentra cada uno de los procesadores (ON,ACTIVE,IDLE,STAND_BY).

**getTerminado():** Retorna un booleano que será true en caso de que haya culminado el procesamiento. Esto se da en el caso de que la suma de las tareas procesadas por ambos procesadores sea igual al máximo indicado, y que el estado de ambos procesadores sea “STAND_BY” 

El resto de las funciones que encontramos en esta clase serán getters y setter, como también encontramos algunas funciones de impresión.

## Análisis de T-Invariantes y justificación de hilos

Utilizando la herramienta PIPE se obtuvo el siguiente análisis de los T-invariantes 

![](img/Picture14.png)

Podemos observar 4 T-invariantes, de los cuales dos serán de un core del CPU y dos del otro. Como mencionamos previamente, dos de ellos corresponden a CPUPower y dos a CPUTask. 

Analizando primero los de CPUPower, que se encarga del encendido y poner en stand- by, nos quedarían los siguientes T-invariantes: 

- T0 -> T1 -> T7 -> T5 -> T2 -> T3 -> T4 siendo el equivalente a 

Arrival\_rate  -> Arrival\_Data\_1 -> stand\_by\_dl1 -> Pw\_up\_dl1 -> st\_ch1 -> Service\_rate\_1 -> Pwr\_dwn\_th1 

- T0 -> T8 -> T14 -> T12 -> T9 -> T10 -> T11 siendo el equivalente a  

Arrival\_rate  -> Arrival\_Data\_2 -> stand\_by\_dl2 -> Pw\_up\_dl2 -> st\_ch2 -> Service\_rate\_2 -> Pwr\_dwn\_th2 

Siendo entonces las secuencias que siguen: 

**Arrival\_rate** como el tiempo que se demora en generar un dato para cualquiera de los dos procesadores, **Arrival\_Data** la llegada de un dato al buffer de uno de los dos procesadores. Estas dos transiciones serán tratadas por el hilo que se encarga de generar los datos.  

Luego el procesador se encuentra en **stand\_by\_dl** y pasa a prenderse en **Pw\_up\_dl**. Luego después de **st\_ch1** pasará a procesarse el dato y el procesador pasa a ACTIVO hasta que se dispara **sv\_rt** que es el tiempo que tarda el procesador en procesar el dato, luego pasa al estado IDLE. En caso de que no haya más elementos para procesar, se dispara **Pwr\_dwn\_th**  

Luego los T-invariantes correspondientes con CPUTask serían: 

- T0 -> T1 -> T6 -> T2 -> T3 siendo el equivalente a  

Arrival\_rate -> Arrival\_Data1 -> stay\_on1 -> st\_ch1 -> Service\_rate\_1 

- T0 -> T8 -> T13 -> T9 -> T10 siendo el equivalente a  

Arrival\_rate -> Arrival\_Data2 -> stay\_on2 -> st\_ch2 -> Service\_rate\_2 

En esta secuencia se repiten las transiciones **Arrival\_rate** y **Arrival\_Data**, estas dos como habíamos mencionado anteriormente son tratadas por el hilo del generador de datos, con lo cual no presentarán problemas. Por otro lado, **st\_ch** y **sv\_rt** serán disparadas por ambos hilos, entonces serán tratadas por el monitor para que solo uno de los dos hilos dispare estas transiciones, sumado a que cada hilo tratara una situación diferente del sistema, **CPUPower** las tratara cuando no haya más elementos en el buffer y **CPUTask** cuando sí haya. 

Dentro de estos T-invariantes, las **stay\_on** son las más importantes, se disparan para mantener el procesador encendido y continuar el tratado de los datos del buffer. 

Analizando esto, llegamos a la conclusión que tendremos 5 hilos activos: 

**GeneradorDatos:** Encargado de generar los datos que serán enviados a alguno de los dos procesadores de acuerdo con lo que le indique la política 

**CPUPower1:** Encargados de los estados CPU\_ON y Stand\_by del procesador número 1, generando así un menor consumo de energía 

**CPUTask1:** Encargado de mantener encendido el procesador número 1 en el caso de tener elementos en el buffer mientras se está procesando algún dato. 

**CPUPower2:** Simil CPUPower1 pero para el procesador numero 2 **CPUTask2:** Simil CPUTask1 pero para el procesador número 2 

También vale mencionar que tendremos dos hilos mas corriendo

**Main:** Se encarga del control general del programa 

**Log:** Para crear un archivo de texto con la información de la ejecución del programa 

## Red de Petri e Hilos Implementados

![](img/Picture15.png)

## Análisis del cumplimiento de los invariantes mediante expresiones regulares

Para hacer el análisis de los T-invariantes se utilizó un Log donde guardamos en un archivo de texto las transiciones que fueron disparadas, luego mediante expresiones regulares se fue comprobando el correcto funcionamiento 

Vocabulario o Alfabeto: 

V = {T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14} Las palabras del alfabeto serán los invariantes de transición: 

- T0T17T5T2T3T4 
- T0T1T6T2T3 
- T0T8T14T12T9T10T11 
- T0T8T13T9T10 
- (Y todos sus prefijos y repeticiones) 

Entonces: 

L = { (T0T17T5T2T3T4 + T0T1T6T2T3 + T0T8T14T12T9T10T11 + T0T8T13T9T10) \*} 

Se tiene entonces la variable inicial S el conjunto de producciones P, el conjunto de terminales Vt = V y un conjunto de no terminales {S, U, V, W, X, Y, Z} constituyendo así la gramática: 

G = ({S, U, V, W, X, Y, Z}; {T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14}; P ; S) 

Donde las producciones P son: 

S -> T0U + T0V

U -> T1W + T1X 

V -> T8Y + T8Z 

W -> T7T5T2T3T4 

X -> T6T2T3 

Y -> T14T12T9T10T11 Z -> T13T9T10 

Z -> T13T9T10

Esta es una gramática del tipo 3 o regular y dado que, si hay al menos una gramática tipo 3 que genera al lenguaje, este lenguaje L es regular.

Debido a la elección de hilos, las transición T6 puede ocurrir antes, entremedio o después de la secuencia T2T3 al igual que T13 con la secuencia T9T10, con lo cual la expresión regular resulta:

T0-(.*?)(T1-(.*?)((((T6-(.*?)))T2-(.*?)T3-)|(T2-(.*?)((T6-(.*?)))T3-)|(T2-(.*?)T3-((.*?)T6-))|(T7-(.*?)T5-(.*?)T2-(.*?)T3-(.*?)T4-))|T8-(.*?)((((T13-(.*?)))T9-(.*?)T10-)|(T9-(.*?)((T13-(.*?)))T10-)|(T9-(.*?)T10-((.*?)T13-))|(T14-(.*?)T12-(.*?)T9-(.*?)T10-(.*?)T11-)))

Dónde (.*?) significa cualquier secuencia intermedia (pues debido a que los hilos pueden dispararse en cualquier momento las palabras del alfabeto no se encontrarán en orden).

Ejemplo:

line = re.subn('a(.*?)b', '', line[0].rstrip()) reemplazará a ‘a’ , ‘b’ y todo lo que esté entre medio por “” (es decir, lo eliminará).
line = re.subn('a(.*?)b', \g<1>'', line[0].rstrip()) reemplazará a “a” , “b” y todo lo que esté entre medio (llámese X) por X, es decir por todo lo que esté entre medio de las expresiones ‘a’ y ‘b’.

Para comprobar que el disparo de transiciones cumple con los invariantes, se guarda el contenido del archivo “tinvariantes.txt”, donde se imprimieron todas las transiciones disparadas a lo largo de la ejecución del programa en una variable y mediante la herramienta regex de Python se irán eliminando secuencias de transiciones que correspondan a los invariantes, si no queda ninguna transición en la variable una vez hecho esto significa que todas las transiciones se ejecutaron en orden, cumpliendo así con los T-invariantes. Para hacer esto se tiene en cuenta que, dado que hay múltiples hilos, las transiciones no aparecen en orden en el archivo “tinvariantes.txt”, con lo cual se debe buscar las secuencias de transiciones en orden (transiciones de interés) pero con otra secuencia de transiciones entre medio de las transiciones de interés, eliminar las transiciones de interés y dejar solamente las secuencias de transiciones intermedias. Luego estas últimas generarán nuevos grupos de transiciones de interés en orden y así sucesivamente hasta eliminar todas. Para esto se utilizó la función subn de la librería re de Python. 

Utilizaremos una estructura line = [file.readline() , 0] que permitirá reemplazar de line[0] las coincidencias con la expresión por los grupos de transiciones intermedias ((.*?) -> g<\n>) y guardará en line[1] la cantidad de coincidencias. Cuando no haya más coincidencias, terminará el bucle.

### Gráfico de la expresión Regular

![](img/Picture16.png)

## Análisis de P-Invariantes

Utilizando la herramienta PIPE se obtuvo el siguiente análisis de los P-invariantes 

![](img/Picture17.png)

Siendo: 

- P0: Data\_gen 
- P1: Data\_ready 
- P3: Active1 
- P4: Idle1 
- P5: CPU\_ON\_1 
- P6: Power\_Up\_1 
- P8: Stand\_by\_1 
- P10: Active2 
- P11: Idle2 
- P12: CPU\_ON\_2 
- P13: Power\_Up\_2 
- P15: Stand\_by\_2 

Entonces, en base a esto podemos decir que: 

- Se genera solo un dato a la vez 
- Los procesadores no pueden estar activos y ociosos al mismo tiempo
- Los procesadores no pueden estar apagados, encendiéndose y encendidos a la vez 

Para comprobar el cumplimiento de los mismos se utiliza la función pinvariantTest() de la clase PetriNet 

![](img/Picture18.png)

Como vemos en la función, se comprobará una a una estas condiciones, en caso de fallar alguna, se pondrá en false la  variable pinvariant y se agregara el archivo del log un mensaje de fallo especificando donde ocurrió el mismo y en qué momento. En caso de no haber ningún fallo, la variable pinvariant permanecerá en true y sabremos que pasó el test 

## Conclusiones

Hemos concluido el trabajo práctico exitosamente. Sin embargo, a lo largo del mismo se presentaron varias complicaciones que requirieron un cambio en el diseño y a partir de cada una se obtuvo un aprendizaje, se detallan algunas de ellas:
- Dado que las transiciones T6 y T13 eran disparadas por hilos que no disparaban ninguna otra transición y sólo requerían que los procesadores se encuentren en estado “ON” para dispararse, cabía la posibilidad de que se acumularan sus disparos hasta luego de haber procesado las tareas (Ejemplo: … T2T3T6T6T6T6T6TT6T6T6T6). Si este comportamiento se hubiese dado, es probable que el análisis de T invariantes mediante la expresión regular planteada hubiese fallado. Esto nos deja un criterio de que, aunque se obtenga un funcionamiento correcto muchas veces (específicamente se probó el correcto funcionamiento para 30 pruebas con 10000 tareas, 30 pruebas con 1000 tareas y 30 pruebas con 300 tareas y con distintos service rates cada 5 pruebas en todos los casos) cabe la posibilidad de que alguna situación no haya sido contemplada en el diseño y pueda fallar alguna vez, por lo que se debe pensar en situaciones que puedan “romper” con el funcionamiento deseado.
- Fue necesario re definir la cantidad de hilos y qué transiciones dispara cada uno en base a las condiciones de la red de Petri, en términos de qué transiciones no deben ponerse en el mismo hilo que otras, pues pueden demorar el disparo de estas y teniendo cuidado de no poner transiciones compartidas en los hilos ya que esto puede dar lugar a situaciones indeseadas.
- Otro cambio importante realizado fue la implementación de transiciones temporizadas pues nuestra implementación inicial no era la correcta. La implementación correcta es:
    - Se debe obtener el estado de las transiciones temporizadas (transición sensibilizada/no sensibilizada) antes y después del disparo de toda transición (con la actualización de estado de las transiciones que ello implica) para detectar cuando una transición temporizada no estaba sensibilizada y luego del disparo si lo está.
    - En esta situación se debe guardar el “Time Stamp” (ejemplo la hora actual) y así se sabrá en qué momento se sensibilizó la transición.
    - Cuando una transición temporizada llega al monitor y está sensibilizada se debe analizar el time stamp y la ventana de dicha transición para determinar si ya se puede o no dispararla. En particular si alpha (tiempo que debe esperar la transición para dispararse una vez sensibilizada) es mayor a el tiempo actual menos el time stamp registrado, entonces la transición NO podrá dispararse. En esta situación se deberá calcular el tiempo restante que se debe esperar para disparar la transición (alpha menos el tiempo actual más el time stamp), liberar la exclusión mutua y dormir al hilo que intentó disparar la transición durante dicho tiempo restante. Una vez transcurrido ese tiempo se competirá por el mecanismo de exclusión mutua nuevamente y se continuará con la ejecución.
    - En todo este proceso se debe tener en cuenta que si existe algún mecanismo que despierte hilos (como era en nuestro caso), se le debe indicar que NO despierte a los hilos que se encuentren durmiendo por cuestiones de temporización.
- Otro requisito que mal interpretamos fue el de la política. Teniendo en cuenta que utilizamos un monitor de concurrencia, un reentrantLock y colas de condición, en un principio interpretábamos que la política debía indicar qué transición disparar en caso de conflicto. En realidad, lo correcto resultó guardar un indicador de los hilos que hayan intentado disparar una transición y que la política determine cuál de ellos despertar (y no solo en caso de conflicto).
- Un error que cometimos y que era difícil de detectar, pues no podíamos visualizar su impacto fue guardar en una variable fuera del monitor la cantidad de tareas en los buffers de cada CPU (que correspondía a las marcas de ciertas plazas de la red de Petri). Esto era un dato inconsistente pues podía modificarse el marcado por otro hilo sin que el hilo que guarda dicha variable tenga conocimiento de esto.